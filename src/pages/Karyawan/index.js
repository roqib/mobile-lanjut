import React from 'react'
import { StyleSheet, Image, Text, View, ScrollView} from 'react-native'
import profil1 from "../../assets/images/profil1.jpg"
import profil2 from "../../assets/images/profil2.jpg"
import profil3 from "../../assets/images/profil3.jpg"
import profil4 from "../../assets/images/profil4.jpg"
import profil5 from "../../assets/images/profil5.jpg"
import profil6 from "../../assets/images/profil6.jpg"

const Foto = (props) => {
  return (
    <View style={styles.isi}>
      <Image 
        source={props.gambar} 
        style={styles.gambar}
      />
      <Text style={styles.nama}>{props.nama}</Text>
      <Text style={styles.jabatan}>{props.jabatan}</Text>
    </View>
  )
}

const Karyawan = (props) => {
  return (
    <View>
      <Text style={styles.judul}>STRUKTUR </Text>
      <ScrollView>
        <View style={styles.wadah}>
          <Foto nama="FURKON" jabatan="KEPALA" gambar={profil1}/>
          <Foto nama="ROQIB" jabatan="WAKIL K" gambar={profil2}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="DOIFY" jabatan="Ke Anggotaan" gambar={profil3}/>
          <Foto nama="Anisa" jabatan="Pelayanan" gambar={profil4}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="Melia" jabatan="Bendahara" gambar={profil5}/>
          <Foto nama="Aisyah" jabatan="Administator" gambar={profil6}/>
        </View>
      </ScrollView>
    </View>
  )
}

export default Karyawan

const styles = StyleSheet.create({
  judul:{
    alignSelf:'center',
    marginVertical:10,
    fontWeight:'bold',
  },
  wadah:{
    width:'100%',
    height:130,
    flexDirection:'row',
    padding:10,
    // borderWidth:1,
  },
  isi:{
    flex:1,
    marginHorizontal:5,
    height:'100%',
    // borderWidth:1,
  },
  gambar:{
    width:'100%',
    height:78,
    borderRadius:5,
  },
  nama:{
    fontWeight:'bold',
    color:'black',
    letterSpacing:2,
  },
  jabatan:{
    fontSize:10,
    letterSpacing:1,
    color:'grey'
  }
})