import { StyleSheet, Text, View } from 'react-native'
import React, {useState} from 'react'
import Icon from 'react-native-vector-icons/FontAwesome';

const Tombol = () => {
  const [focused, setFocused] = useState ('#1e88e5');
  const onPressFunction = () => {
    let color = "rgb(" +
      Math.floor(Math.random()*256) +
      "," +
      Math.floor(Math.random()*256) +
      "," +
      Math.floor(Math.random()*256) +
      ")";
     setFocused(color); 
  }
  return (
    <View style={styles.wadah}>
        <Icon.Button name="lock" onPress={onPressFunction} style={{backgroundColor:focused}}>
          <Text style={styles.text2}>Ubah Warna</Text>
        </Icon.Button>
      </View>
  )
}

export default Tombol

const styles = StyleSheet.create({
    // tombol:{
    //     backgroundColor:'#1e88e5',
    //   },
      wadah:{
        marginVertical:55,
        alignItems:'center',
      },
      text2:{
        color:'white',
        fontWeight:'bold',
        fontSize:15,
      },
})