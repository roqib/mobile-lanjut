import React, {useEffect, useState} from 'react';
import Karyawan from '../pages/Karyawan';
import Home from '../pages/Home';
import Splash from '../pages/Splash';
import Profil from '../pages/Profil';
import Animasi from '../pages/Animasi'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();


const TabBebe = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Home} options={{headerShown:false, tabBarIcon:({color, size})=>(<Icon name='home' size={size} color={'orange'} />)}} />
      <Tab.Screen name="Struktur" component={Karyawan} options={{headerShown:false, tabBarIcon:({color, size})=>(<Icon name='users' size={size} color={'orange'} />)}}/>
      <Tab.Screen name="Contact" component={Animasi} options={{headerShown:false, tabBarIcon:({color, size})=>(<Icon name='rocket' size={size} color={'orange'} />)}}/>
      <Tab.Screen name="anggota" component={Profil} options={{headerShown:false, tabBarIcon:({color, size})=>(<Icon name='user' size={size} color={'orange'} />)}}/>
    </Tab.Navigator>
  );
}

const Router = () => {
  const [show, setShow] = useState (true)
  useEffect(()=> {
    setTimeout(() => {
      setShow(false)
    }, 3000)
  })
  return(
    <Stack.Navigator>
      {show ? (
        <Stack.Screen name="Splash" component={Splash} options={{headerShown:false}}/>
      ): null}
      <Stack.Screen name="Main" component={TabBebe} options={{headerShown:false}}/>
      <Stack.Screen name="Home" component={Home} options={{headerShown:false}}/> 
    </Stack.Navigator>
  )
}

export default Router
