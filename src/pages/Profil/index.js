import { StyleSheet, Button, Text, View } from 'react-native'
import React, {useState, Component} from 'react'

const Angka = () => {
    const [nomor, setNomor]= useState(0);
    return (
        <View>
        <Text style={styles.judul2}>Dengan Function</Text>
            <Text style={styles.text}>Angka sekarang adalah {nomor}</Text>
            <Button title='Tambah' color={'orange'} onPress={() => setNomor(nomor+1)}/>
        </View>
    )
}

class AngkaBaru extends Component {
    state ={
        nomor: 0
    }
    render() {
        return (
            <View>
                <Text style={styles.judul2}>Dengan Component</Text>
                <Text style={styles.text}>Angka sekarang adalah {this.state.nomor}</Text>
                <Button title='Tambah' color={'orange'} onPress={() => this.setState({nomor: this.state.nomor+1})}/>
            </View>
        )
    }
}

const Profil = () => {
  return (
    <View style={styles.wadah}>
      <Text style={styles.judul}>Dinamis dengan State</Text>
      <Angka/> 
      <AngkaBaru/> 
    </View>
  )
}

export default Profil

const styles = StyleSheet.create({
    judul:{
        alignSelf:'center',
        fontWeight:'bold',
        marginVertical:10,
    },
    judul2:{
        fontWeight:'bold',
        marginVertical:10,
    },
    wadah:{
        padding:20,
    },
    text:{
        alignSelf:'center',
        marginVertical:5,
    }
})